'use strict';

const express = require('express');
var client  = require('./config/db');

const PORT = 8080;
const HOST = '0.0.0.0';
const app = express();

app.get('/', (req, res) => {
  res.send('Hello World');
});

app.get("/users",(req,res) => {
  client.query('SELECT * FROM users' , function (err, result) {
    if (err) {
        console.log(err);
        res.status(400).send(err);
    }
    res.status(200).send(result.rows);
  });
});

app.listen(PORT, function () {
  console.log('Server is running.. on Port ' + PORT);
});