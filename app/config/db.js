'use strict';

const { Client } = require('pg');

const client = new Client({
    user: 'admin',
    host: '10.100.173.229',
    database: 'db-loka',
    password: 'admin123',
    port: 5432,
});

client.connect(err => {
    if (err) {
      console.error('connection error', err.stack)
    } else {
      console.log('connected')
    }
  })

module.exports = client;
