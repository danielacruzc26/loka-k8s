terraform {
  backend "s3" {
    bucket = "loka-k8s-terraform"
    key    = "loka-k8s"
    region = "us-east-1"
  }
}