locals {
  region = "us-east-1"
  availability_zones = ["us-east-1a", "us-east-1b"]
  namespace = "loka"
  name = "eks"
  kubernetes_version = "1.17"
  instance_types = ["t3.small"]
  desired_size = 2
  max_size = 2
  min_size = 2
  disk_size = 20
}