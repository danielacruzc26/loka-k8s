# Test

## Requirements

* Terraform
* aws-cli
* docker
* helm

## About this project

* Bucket with versioning to save terraform state
* Create EKS cluster with 2 worker nodes
* /app folder contain node web application
* /eks-cluster contain terraform code to create an EKS cluster
* /k8s contain kubernetes templates objects in yaml format
* Nodejs application
* Database PostpreSQL
* Jenkins
* Fluent-bit to sent data to Elasticsearch service AWS

## Create EKS cluster

1. Customize valiables to create cluster in `default_variables.tf` file
2. Execute terraform commands to create resources

```sh
terraform init
terraform plan
terraform apply
```

## Install Helm-Operator 

Create namespace `fluxcd`

```sh
kubectl create ns fluxcd
```

Deploy helm-operator chart:

```sh
helm upgrade -i helm-operator fluxcd/helm-operator --wait \
--namespace fluxcd \
--set helm.versions=v3
```

## Install PostgreSQL
Using `Helm-Operator` create Helmrelease:

```sh
kubectl apply -f k8s/helmrelease-postgresql.yaml
```

### Create test table and inserts
```sh
CREATE TABLE users(id SERIAL PRIMARY KEY, name TEXT NOT NULL);
INSERT INTO users(name) VALUES ('Daniela');
INSERT INTO users(name) VALUES ('Julia');
INSERT INTO users(name) VALUES ('Victor');
INSERT INTO users(name) VALUES ('Jonathan');
INSERT INTO users(name) VALUES ('Fernando');
```

## Install Jenkins
Using `Helm-Operator` create Helmrelease:

```sh
kubectl apply -f k8s/helmrelease-jenkins.yaml
```

## Install Fluent-bit
Using `Helm-Operator` create Helmrelease:

```sh
kubectl apply -f k8s/helmrelease-fluent-bit.yaml
```

## Install NodeJS app
```sh
kubectl apply -f k8s/app-deployment.yaml
kubectl apply -f k8s/app-service.yaml
```